#!/bin/bash

#
# File .......... create_vivado_project.sh
# Author ........ Steve Haywood
# Version ....... 1.0
# Date .......... 03 May 2021
# Description ...
#   Very simple script to launch Vivado and run the TCL script. Should be run
# in the project host directory that contains the fw & sw subdirectories, for
# example :-
#
# user@host:~/projects/project$ create_vivado_project.sh
# user@host:~/projects/project$ create_vivado_project.sh build
#

# Get command line arguments
if [ $# == 1 ]; then
  arg1=$1
else
  arg1=""
fi

# Get script location
dir_script=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Get present working directory
dir_pwd=$(pwd)

# Set directory structure
dir_base="fw"
dir_fw_project="vivado"

# Sanity check directory structure
if [ -d "${dir_pwd}/${dir_base}" ]; then
  if [ ! -e "${dir_pwd}/${dir_base}/${dir_fw_project}" ]; then

    # Launch Vivado
    if [ "$arg1" == "build" ]; then
      vivado -nojournal -nolog -notrace -mode gui -source "${dir_script}/create_vivado_project.tcl" -tclargs build &
    else
      vivado -nojournal -nolog -notrace -mode gui -source "${dir_script}/create_vivado_project.tcl" &
    fi

  else
    echo "Error: Firmware project directory \"${dir_base}/${dir_fw_project}\" already exists, exiting."
  fi
else
  echo "Error: Firmware base directory \"${dir_base}\" does not exists, exiting."
fi
