#
# File .......... create_vivado_project.tcl
# Author ........ Steve Haywood
# Version ....... 1.0
# Date .......... 23 December 2021
# Description ...
#   Simple Tcl script to create a new Vivado project, import external
# sources and (optionally) generate the bitstream & export the hardware.
#

puts "Set Global variables"
set dir_firmware    "fw/vivado"
set dir_user        "../src"
set dir_diagram     "$dir_user/diagram"
set dir_design      "$dir_user/design"
set dir_ip          "$dir_user/ip"
set dir_ip_repo     "$dir_user/ip_repo"
set dir_constraint  "$dir_user/constraint"
set dir_testbench   "$dir_user/testbench"
set dir_script      "$dir_user/script"

puts "NOTE: Create project directory, project & change PWD"
create_project project $dir_firmware -part xc7z020clg484-1
cd $dir_firmware

puts "NOTE: Add local IP Repository"
if {[file exist $dir_ip_repo]} {
  set_property  ip_repo_paths  $dir_ip_repo [current_project]
  update_ip_catalog
}

puts "NOTE: Add scripts"
add_files -quiet -norecurse -fileset utils_1 $dir_script
set tcls [get_files *.tcl]
foreach tcl $tcls {
  set tcl_name [file tail $tcl]
  switch $tcl_name {
    "pre_synth.tcl" {
      set_property STEPS.SYNTH_DESIGN.TCL.PRE [ get_files $tcl -of [get_fileset utils_1] ] [get_runs synth_1]
    }
    "post_bit.tcl" {
      set_property STEPS.WRITE_BITSTREAM.TCL.POST [ get_files $tcl -of [get_fileset utils_1] ] [get_runs impl_1]
    }
    default {
      puts "NOTE: Unfamiliar tcl script found - $tcl_name"
    }
  }
}

puts "NOTE: Add block designs"
add_files -quiet -norecurse [glob -nocomplain "$dir_diagram/*/*.bd"]

puts "NOE: Add design sources"
add_files -quiet -norecurse $dir_design

puts "NOTE: Add IPs"
add_files -quiet [glob -nocomplain "$dir_ip/*/*.xci"]

puts "NOTE: Add constraints"
add_files -quiet -norecurse -fileset constrs_1 $dir_constraint

puts "NOTE: Add testbench sources"
add_files -quiet -norecurse -fileset sim_1 $dir_testbench

puts "NOTE: Recreate (by opening) all block designs..."
set bds [glob -nocomplain "$dir_diagram/*/*.bd"]
foreach bd $bds {
  puts "NOTE: Open \"$bd\" block design"
  open_bd_design $bd
}

puts "NOTE: Generate all (none-overwrite) block designs wrappers..."
set bds [get_bd_designs -quiet]
foreach bd $bds {
  puts "NOTE: Generate \"$bd\" block design wrapper"
  set bd_file [get_files $bd.bd]
  set bd_path [file dirname $bd_file]
  set wrapper_files [glob -nocomplain $bd_path/hdl/${bd}_wrapper.*]
  if {[llength $wrapper_files] == 0} {
    set wrapper_file [make_wrapper -files $bd_file -top]
  }
  add_files -quiet -norecurse $bd_path/hdl
}

puts "NOTE: Close all block designs..."
set bds [get_bd_designs -quiet]
foreach bd $bds {
  puts "NOTE: Close \"$bd\" block design"
  close_bd_design $bd
}

puts "NOTE: Check for build argument..."
if { $argc == 1 } {
  set arg0 [lindex $argv 0]
  if { $arg0 == "build" } {
    puts "NOTE: Generate bitstream & export hardware"
    launch_runs impl_1 -to_step write_bitstream -jobs 4
    wait_on_run impl_1
    write_hw_platform -fixed -include_bit -force -file ../system_wrapper.xsa
  }
}

puts "NOTE: That's all folks!"
