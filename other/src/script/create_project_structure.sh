#!/bin/bash


#
# File .......... create_project_structure.sh
# Author ........ Steve Haywood
# Version ....... 1.0
# Date .......... 29 December 2021
# Description ...
#   Simple script to create a possible project directory structure for combined
# firmware, hardware, operating system & software development. Very much work
# in progress and by all means not a golden solution to anything.
#


# Print instructions
function print_args {
  printf "\n"
  printf "Create project (FW, HW, OS & SW) directory structure\n"
  printf "\n"
  printf "Usage: %s dir\n" $1
  printf "\n"
  printf "Where: dir .... Project directory to create & populate\n"
  printf "\n"
  printf "Example: %s ~/projects/example_project\n" $1
  printf "Example: %s /usr/projects/hello_world\n" $1
  printf "\n"
  exit 1
}


# Deal with command line arguments
if [ $# != 1 ]; then
  print_args $0
fi


# Get project base directory
base=$1


# Echo operation
echo "Creating project directory structure @ ${base}"


# Project directories
declare -a dirs=(
  # Firmware
  "fw/src/constraint"
  "fw/src/design"
  "fw/src/diagram"
  "fw/src/document"
  "fw/src/ip"
  "fw/src/ip_repo"
  "fw/src/other"
  "fw/src/script"
  "fw/src/testbench"
  "fw/vivado"
  # Hardware
  "hw/src/schematic"
  # Operating System
  "os/src/other"
  # Software
  "sw/src/c"
  "sw/src/other"
  "sw/src/script"
  "sw/vitis"
)


# Iterate through directories
for dir in "${dirs[@]}"
do
#  echo "Creating ... ${base}/${dir}"
  mkdir -p "${base}/${dir}"
done


# Create C source starter file
touch "${base}/sw/src/c/$(basename $base).c"
