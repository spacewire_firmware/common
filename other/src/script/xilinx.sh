#!/bin/bash

#
# File .......... xilinx.sh
# Author ........ Steve Haywood
# Version ....... 1.0
# Date .......... 8 February 2021
# Description ...
#   Determine and list which Xilinx tools are available and let the user select
# the require ones to use. Does not reverse any previously selected tools upon
# selecting news ones so to is cleaner to use in a new Terminal session.
#

# Define install base location
dir_base="/opt/Xilinx"
#dir_base="/mnt/Xilinx"

# Define number of tools (Vivado, SDK, Vitis & PetaLinux)
tools=4

# Define tool directories, executables & settings scripts
dir_tool=("Vivado" "SDK" "Vitis" "PetaLinux")
exe_tool=("vivado" "xsdk" "vitis" "petalinux-build")
set_tool=("settings64.sh" "settings64.sh" "settings64.sh" "tool/settings.sh")







# MY OVERRIDE
#dir_base="/media/secondary/Xilinx"
#set_tool=("settings64.sh" "settings64.sh" "settings64.sh" "settings.sh")







# Define terminal colours
col_default=$(tput sgr0)
col_red=$(tput setaf 1)
col_green=$(tput setaf 2)
col_blue=$(tput setaf 4)
col_fail_pass=(${col_red} ${col_green})

# Exit if script wasn't sourced
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  printf "Script needs to be sourced : source ${BASH_SOURCE[0]}\n"
  exit 1
fi

# Get array of directories at base location
readarray -t dirs < <(find "${dir_base}/${dir_tool[0]}" -mindepth 1 -maxdepth 1 -printf '%P\n')

# Sort directories array
dirs=($(IFS=$'\n'; echo "${dirs[*]}" | sort))

# Determine and list what tools are installed
declare -A table
printf "Xilinx tools available tools at ${dir_base} :-\n"
for ((dir=1; dir<=${#dirs[@]}; dir++)); do
  printf "${dir}) ${dirs[$((dir-1))]}"
  for ((tool=0; tool<${tools}; tool++)); do
    table[${dir}, ${tool}]=$([ ! -d "${dir_base}/${dir_tool[${tool}]}/${dirs[$((dir-1))]}" ]; echo $?)
    printf "${col_default} - "
    printf "${col_fail_pass[${table[${dir}, ${tool}]}]}"
    printf "${dir_tool[${tool}]}"
    printf "${col_default}"
  done
  printf "\n"
done
printf "0) Exit\n"
printf "Please select tools required or exit"

# Obtain user input and source settings for selected tools
while true; do
  read -p " : " sel
  if ! [[ "${sel}" =~ ^[0-9]+$ ]] ; then
    # User input is not a number
    printf "Invalid choice, select again"
  elif [ ${sel} -ge 1 ] && [ ${sel} -le ${#dirs[@]} ]; then
    # User input is out of range
    printf "\nTools are as follows :-\n" "${sel}"
    for ((tool=0; tool<${tools}; tool++)); do
      if [ ${table[${sel}, ${tool}]} -eq 1 ]; then
        source "${dir_base}/${dir_tool[${tool}]}/${dirs[$((sel - 1))]}/${set_tool[${tool}]}" > /dev/null 2>&1
        printf "${col_blue}${exe_tool[${tool}]}${col_default} @ "
        bin=$(which "${exe_tool[${tool}]}")
        if [ -z ${bin} ]; then
          printf "${col_red}Not found!"
        else
          printf "${col_green}%s" "${bin}"
        fi
        printf "${col_default}\n"
      fi
    done
    break
  elif [ ${sel} -eq 0 ]; then
    printf "Quiting without selection!\n"
    break
  else
    printf "Invalid choice, select again"
  fi
done
