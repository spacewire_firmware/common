#!/bin/bash

#
# File .......... petalinux-build-id.tcl
# Author ........ Steve Haywood
# Version ....... 1.0
# Date .......... 2 October 2022
# Description ...
#   Script to read project information file, obtain GIT repository information,
# append project information file with GIT information, build PetaLinux project
# and then restore project information file.
#
#

# Set identification file
project="project-spec/meta-user/recipes-apps/website/files/project.txt"

# Only perform bulk of actions if identification file
if [ -f "$project" ]; then

  # Read project information
  readarray -t ids < project-spec/meta-user/recipes-apps/website/files/project.txt

  # Display identification information
  echo -e '\033[1mNOTE\033[0m: Building PetaLinux with the following identification'
  echo "Description ... ${ids[0]}"
  echo "Company ....... ${ids[1]}"
  echo "Author ........ ${ids[2]}"

  # Get GIT timestamp
  id_timestamp=$(git log -1 --pretty=%cd --date=format:"%d-%b-%Y - %H:%M:%S")
  if [ $? -ne 0 ]; then
    id_timestamp="00-Xxx-0000 - 00:00:00"
  fi

  # Get GIT hash
  id_hash=$(git log -1 --pretty=%H)
  if [ $? -ne 0 ]; then
    id_hash="0000000000000000000000000000000000000000"
  fi

  # Get GIT status
  id_append=""
  status=$(git status -s)
  if [ $? -ne 0 ]; then
    id_append="(undefined)"
  else
    if [ ! -z "$status" ]; then
    	id_append="(unstaged)"
    else
      status=$(git branch -r --contains ${id_hash})
      if [ $? -ne 0 ]; then
        id_append="(undefined)"
      else
        if [ -z "$status" ]; then
          id_append="(unpushed)"
        fi
      fi
    fi
  fi

  # Display identification information
  echo "Version ....... ${ids[3]} ${id_append}"
  echo "Timestamp ..... $id_timestamp"
  echo "Hash .......... $id_hash"

  # Modify project information file
  fname="$project"
  echo ${ids[0]}               > ${fname} # Description
  echo ${ids[1]}              >> ${fname} # Company
  echo ${ids[2]}              >> ${fname} # Author
  echo ${ids[3]} ${id_append} >> ${fname} # Version
  echo ${id_timestamp}        >> ${fname} # Timestamp
  echo ${id_hash}             >> ${fname} # Hash

  # Build PetaLinux
  $(which petalinux-build)
  jobs -l
  wait

  # Restore project information file
  echo ${ids[0]}  > ${fname} # Description
  echo ${ids[1]} >> ${fname} # Company
  echo ${ids[2]} >> ${fname} # Author
  echo ${ids[3]} >> ${fname} # Version

else

  # Build PetaLinux
  $(which petalinux-build)

fi
