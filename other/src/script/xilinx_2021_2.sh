#!/bin/bash

# Setup Vitis, Vivado, Vitis HLS & DocNav environment
source /opt/Xilinx/Vivado/2021.2/settings64.sh
# Setup PetaLinux environment
source /opt/Xilinx/PetaLinux/2021.2/tool/settings.sh
