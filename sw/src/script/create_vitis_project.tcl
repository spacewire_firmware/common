#
# File .......... create_vitis_project.tcl
# Author ........ Steve Haywood
# Version ....... 1.0
# Date .......... 21 Dec 2021
# Description ...
#   Simple Tcl script to create a new Vitis application, import external
# sources and (optionally) build the application.
#

# Check for project name argument, exit if not found.
if { $argc == 0 } {
  puts "Error: No project name provided, exiting..."
  exit 1
}
set project [lindex $argv 0]

# Create and build new project.
setws sw/vitis
app create -name $project -hw fw/system_wrapper.xsa -os standalone -proc ps7_cortexa9_0 -template {Empty Application(C)}
set abs_path [file normalize sw/src/c]
importsources -name $project -path $abs_path -soft-link
if { $argc > 1 } {
  set arg1 [lindex $argv 1]
  if { $arg1 == "build" } {
    app build -name $project
  }
}
