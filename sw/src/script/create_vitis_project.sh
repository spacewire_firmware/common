#!/bin/bash

#
# File .......... create_vitis_project.sh
# Author ........ Steve Haywood
# Version ....... 1.0
# Date .......... 03 May 2021
# Description ...
#   Very simple script to launch the Xilinx software command-line tool,
# run the TCL script and launch Vitis. Should be run in the project host
# directory that contains the fw & sw subdirectories, for example :-
#
# user@host:~/projects/project$ create_vitis_project.sh
# user@host:~/projects/project$ create_vitis_project.sh build
#

# Get command line arguments
if [ $# == 1 ]; then
  arg1=$1
else
  arg1=""
fi

# Get script location
dir_script=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Get present working directory
dir_pwd=$(pwd)

# Set directory structure
dir_base="sw"
dir_sw_project="vitis"

# Sanity check directory structure
if [ -d "${dir_pwd}/${dir_base}" ]; then
  if [ ! -e "${dir_pwd}/${dir_base}/${dir_sw_project}" ]; then

    # Get last element of path (project name)
    dir_project=${dir_pwd##*/}

    # Call Xilinx Software Commandline Tool
    if [ "$arg1" == "build" ]; then
      xsct "${dir_script}/create_vitis_project.tcl" "${dir_project}" build
    else
      xsct "${dir_script}/create_vitis_project.tcl" "${dir_project}"
    fi

    # Launch Vitis
    vitis -workspace "${dir_base}/${dir_sw_project}" &

  else
    echo "Error: Software project directory \"${dir_base}/${dir_sw_project}\" already exists, exiting."
  fi
else
  echo "Error: Software base directory \"${dir_base}\" does not exists, exiting."
fi
